import serial.tools.list_ports  # For listing available serial ports
import serial
import os, platform



def findUSBPort():
    """This function trys to figure out at which USB port the RFID scanner is
    connected to and then use that one. It is supposed to work on any platform.
    """
    # Figure out the OS we are running on
    osPlatformName = platform.system()
    print("OS is: ", osPlatformName)

    portDeviceName = ''
    myPortID = "0001"
    # List ports for user to select
    serialPortList = serial.tools.list_ports.comports()
    print('\nDetected the following serial ports:')
    for serialPort in serialPortList:
        print('Port:%s\tID#:=%s' % (serialPort.device, serialPort.serial_number))

        if (serialPort.serial_number.__str__()[:len(myPortID)]==myPortID): # Match ID with the correct port
            portDeviceName = serialPort.device # Store the device name to later open port with.
    if len(portDeviceName)!=0:
        print('\r\n%s is the correct port.' %(portDeviceName))
    else:
        print("Port with ID: %s is not found!" %(myPortID))

    print('\n')
    return portDeviceName


def init():
    global ser 
    ser = serial.Serial(findUSBPort(),115200,timeout=0.5)#Change this to "COM3" for most Windows devices

    # Region
    region = {
        "china1": [0xbb, 0x00, 0x07, 0x00, 0x01, 0x04, 0x0c, 0x7e],
        "china2": [0xbb, 0x00, 0x07, 0x00, 0x01, 0x01, 0x09, 0x7e],
        "us": [0xbb, 0x00, 0x07, 0x00, 0x01, 0x02, 0x0a, 0x7e],
        "eu": [0xbb, 0x00, 0x07, 0x00, 0x01, 0x03, 0x0b, 0x7e],
        "korea": [0xbb, 0x00, 0x07, 0x00, 0x01, 0x06, 0x0e, 0x7e],
        }

    ser.write(region["eu"])
    ser.write([0xbb, 0x00, 0xad, 0x00, 0x01, 0xff, 0xad ,0x7e])# FHSS ON
    ser.write([0xbb, 0x00, 0xb6, 0x00, 0x02, 0x07, 0xd0, 0x8f ,0x7e])#Full power


def scan():
    ser.write([0xbb,0,0x22,0,0,0x22,0x7e])
    s = ser.readline()
    name = ""
    
    if(s[1]==2):
        while (len(s)>10):
            pc=s[6:8]
            if(pc[0]==0x30):
                while (len(s)<22):
                    s2 = ser.readline()
                    s = s+s2
                epc=s[8:20]
                name = ''.join(("%0.2X" % x) for x in epc)
                s=s[24:]
                return name
            if(pc[0]==0x18):
                epc=s[8:14]
                name = ''.join(("%0.2X" % x) for x in epc)
                s=s[18:]
                return name
    

# init()
# import time
# while True:
#     print(scan())
#     time.sleep(0.5)

